package dao;

public class Encryption {
   public static String encrypt( String password){
	  String result ="";
	  char ch ;
	  for (int i = 0 ; i<password.length() ; i++){
		ch = password.charAt(i);
		ch += 20*2/5;
		result +=ch;
	}
  return result ;	
} 
   public static String decrypt(String password){
		  String result ="";
		  char ch ;
		  for (int i = 0 ; i<password.length() ; i++){
			ch = password.charAt(i);
			ch -= 20*2/5;
			result +=ch;
		}
	  return result ;	
	}
}
