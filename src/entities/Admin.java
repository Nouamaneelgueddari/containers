package entities;
public class Admin {
	private int  Id ;
	private String login;
	private String password;
	
	
	@Override
	public String toString() {
		return "Admin [Id=" + Id + ", login=" + login + ", password="
				+ password + "]";
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Admin(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}
	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
}
