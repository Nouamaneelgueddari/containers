<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri= "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Location de conteneurs- Bachelor in Computer Engineering</title>
  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
  <!--banner-->
  <section id="banner" class="banner">
    <div class="bg-color">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
              <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-responsive" style="width: 140px; margin-top: -16px;"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
                <li class="active"><a href="/Conteneur/admin_home.php">Home</a></li>
                <li class=""><a href="#contact">Contact</a></li>
                <li class=""><a>Bienvenue ${model.cl.nom} ${model.cl.prenom}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <div class="container">
        <div class="row">
          <div class="banner-info">
            <div class="banner-logo text-center">
              <img src="img/logo.png" class="img-responsive">
            </div>
             <div class="banner-text text-center">
              <h1 class="white">Mieten Sie einen Container</h1>
              <h2 class="white">Die Anmietung eines Containers bei uns ist einfach und unkompliziert.</h2>
              <h3 class="white">Gro�e Auswahl zu unschlagbaren Preisen.</h3>
            </div>
            <div class="overlay-detail text-center">
              <a href="#service"><i class="fa fa-angle-down"></i></a>
            </div>
          </div>
        </div> 
      </div>
    </div>
  </section>
  <!--/ banner-->
   <section id="contact" class="section-padding">
   <div class="col-md-12">
          <h2 class="ser-title">Containerliste</h2>
          <hr class="botm-line">
    </div>
    <div  class ="col-md-10">  	
     <th><a href="Ajouter.php" class="btn btn-default pull-right" >Hinzuf�gen</a></th>
    </div>	  
  	<div class="table-responsive container  ">
  		<table class="table table-bordered">
		  <thead class="thead-dark">
		    <tr>
		      <th scope="col">ID</th>
		      <th scope="col">id_depot</th>
		      <th scope="col">Type</th>
		      <th scope="col">Largeur</th>
		      <th scope="col">Hauteur</th>
		      <th scope="col">Longueur</th>
		      <th scope="col">Largeur de la porte</th>
		      <th scope="col">Hauteur de la porte</th>
		      <th scope="col">Longueur de la porte</th>
		      <th scope="col">Poids</th>
		      <th scope="col">Capacite</th>
		      <th scope="col">Prix</th>
		    </tr>
		  </thead>
		  <tbody> 
		  <c:forEach items="${model.conteneurs}" var="c">
		    <tr>
		      <th scope="col">${c.getId() }</th>
		      <th scope="col">${c.getId_depot()}</th>
		      <th scope="col">${c.getType() }</th>
		      <th scope="col">${c.getLargeur() }</th>
		      <th scope="col">${c.getHauteur() }</th>
		      <th scope="col">${c.getLongueur() }</th>
		      <th scope="col">${c.getLargeur_porte() }</th>
		      <th scope="col">${c.getHauteur_porte() }</th>
		      <th scope="col">${c.getLongueur_porte() }</th>
		      <th scope="col">1${c.getPoids() }22</th>
		      <th scope="col">${c.getCapacite() }</th>
		      <th scope="col">${c.getPrix() }</th>
		      <th><a  href="suprimer.php?id=${c.getId()}"  class="btn btn-default" onclick="return confirm('�tes-vous s�r de bien vouloir supprimer cet �l�ment?');">L�schen</a></th>
		      <th><a  href="Editer.php?id=${c.getId()}" class="btn btn-default" >Ver�nderung</a></th>
		    </tr>
		   </c:forEach>
		  </tbody>
		</table>
</div>
 </section>
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>

</body>

</html>
