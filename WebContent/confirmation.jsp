<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<title>Les Conteneurs</title>
</head>
<body>
<p></p>
<div class="container col-md-6 col-md-offset-3 ">
		<div class="well well-sm">
   				 <div class="panel-heading"> Confirmation d'enregistrement</div>
  		</div>
			<form action="insert.php" method="get">
				<div class="panel-primary">
				<table class="table table-striped">		
					 	<tr>					
							<th><label>Typ:</label></th><th><label>${model.c.getType()}</label></th> 
						</tr>
						<tr>
							<th><label>Breite:</label></th><th><label >${model.c.getLongueur()}</label></th> 
						</tr>
						<tr>
							<th><label>H�he:</label></th><th><label >${model.c.getHauteur()}</label></th> 
						</tr>
						
						<tr>	
							<th><label>L�nge:</label> </th><th><label >${model.c.getLargeur()}</label> </th>
						</tr>
						<tr>	
							<th><label>Breite der T�r:</label> </th><th><label >${model.c.getLargeur_porte()}</label> </th>
						</tr>
						<tr>
							<th><label>H�he der T�r:</label></th><th><label >${model.c.getLongueur_porte() }</label></th>  
						</tr>
						<tr>
							<th><label>Hauteur porte du conteneur:</label></th><th><label >${model.c.getHauteur_porte() }</label></th>  
						</tr>	
						<tr>
							<th><label >Hauteur porte du conteneur:</label></th><th><label >${model.c.getHauteur()}</label> </th>
						</tr>	
						<tr>	
							<th><label >Poids du conteneur :</label></th><th> <label >${model.c.getPoids() }</label> </th>
						</tr>
						<tr>
							<th><label >Capacit� du conteneur:</label></th><th><label >${model.c.getCapacite()}</label></th>  
						</tr>	
						<tr>	
							<th><label>Prix location:</label></th><th><label >${model.c.getPrix()}</label> </th>
						</tr> 
						
				</table>		
					
				</div>	
				<div class="btn-group btn-group-justified">
    				<a href="confirmer.php?type=${model.c.getType()}&lng=${model.c.getLongueur()}&hau=${model.c.getHauteur()}&larg=${model.c.getLargeur()}&larg_p=${model.c.getLargeur_porte()}&Long_p=${model.c.getLongueur_porte()}&Hau_p=${model.c.getHauteur()}&poids=${model.c.getPoids()}&cap=${model.c.getCapacite()}&prix=${model.c.getPrix()}" onclick="return confirm('�tes-vous s�r de bien vouloir enregistrer cet �l�ment?');" class="btn btn-primary">Confirmer</a>
    				<a href="annuler.php?" class="btn btn-primary">Annuler</a>
  				</div>
       </form>

       </div>
  
</body>
</html>