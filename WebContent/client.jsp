<!DOCTYPE html>
<html>
<head>
  <!-- - <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">-->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Location de conteneurs- Bachelor in Computer Engineering</title>
  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
  <!--banner-->
  <section id="banner" class="banner">
    <div class="bg-color">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
              <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-responsive" style="width: 140px; margin-top: -16px;"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#banner">Home</a></li>
                <li class=""><a href="#contact">Contact</a></li>
                <li class=""><a> Willkommen ${model.cl.nom} ${model.cl.prenom}</a></li>
                <li class=""><a href="/Conteneur/client_Deconnexion.php">Deconnexion</a></li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <div class="container">
        <div class="row">
          <div class="banner-info">
            <div class="banner-logo text-center">
              <img src="img/logo.png" class="img-responsive">
            </div>
            <div class="banner-text text-center">
              <h1 class="white">Mieten Sie einen Container</h1>
              <h2 class="white">Die Anmietung eines Containers bei uns ist einfach und unkompliziert.</h2>
              <h3 class="white">Gro�e Auswahl zu unschlagbaren Preisen.</h3>
            </div>
            <div class="overlay-detail text-center">
              <a href="#service"><i class="fa fa-angle-down"></i></a>
            </div>
          </div>
        </div> 
      </div>
    </div>
  </section>
  <!--/ banner-->
  <!--service-->
  <section id="service" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4">
          <h2 class="ser-title">Unser Service </h2>
          <hr class="botm-line">
          <p> Zuh�ren, Beratung und Servicequalit�t ... Grunds�tze des Teams Bachelor in Computer Engineering.</p>
        </div>
        <div class="col-md-4 col-sm-4">
        <form action="consulter_list.php"  method="get">
           <div class="service-info">
            <div class="icon">
              <a  href="/Conteneur/consulter_list.php?"><i class="fa fa-list"></i></a>
            </div> 
            <div class="icon-info">
              <h4>Konsultieren Sie unsere Beh�lter</h4>
              <p>Entdecken Sie die gesamte Palette unserer Beh�lter. Finden Sie den Beh�lter, der Ihren Bed�rfnissen entspricht.</p>
            </div> 
           </div>
           </form>
        </div>
        <div class="col-md-4 col-sm-4">
          <form action="consulter_louer.php"  method="get">
          <div class="service-info">
            <div class="icon">
              <a href="/Conteneur/consulter_louer.php?"><i class="fa fa-lightbulb-o"></i></a>
            </div>
            <div class="icon-info">
              <h4>Mieten Sie einen Container</h4>
              <p>Auf der Suche nach einer Speicherl�sung? Wir kennen Ihre Anforderungen, als anspruchsvoller Verbraucher suchen Sie auch nach einer sicheren Speicherl�sung, wasserdicht und manchmal sogar mobil, sehen sogar K�hlung</p>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!--/ service-->
  <!--about-->
  <section id="about" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12">
          <div class="section-title">
            <h2 class="head-title lg-line">Neue und gebrauchte Container f�r Transport und Lagerung</h2>
            <hr class="botm-line">
            <p class="sec-para">Wir liefern Standard-Seecontainer, K�hlcontainer und Tankcontainer sowie Spezialcontainer wie Open-Top-Container, 
                 Container mit Doppelt�ren und vieles mehr - wir haben alle Arten von Containern, die Sie brauchen!</p>
          </div>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
          <div style="visibility: visible;" class="col-sm-9 more-features-box">
            <div class="more-features-box-text">
              <div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>
              <div class="more-features-box-text-description">
                <h3>Unsere Beh�lter sind in verschiedenen Bedingungen und Ausf�hrungen erh�ltlich:</h3>
                <p>Neue Container k�nnen direkt von unserem Werk in China an jeden Ort der Welt geliefert werden. Die Beh�lter sind in verschiedenen RAL-Farben vorr�tig,
                   k�nnen aber auch in der Farbe Ihrer Wahl lackiert werden.</p>
               <p>Gebrauchte Container sind in gutem Zustand f�r Lagerung und Transport verf�gbar. Sie sind in mehr als 200 Standorten auf der ganzen Welt verf�gbar.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ about-->
  <!--doctor team-->
  <section id="doctor-team" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="ser-title">UNSERE CONTAINERS</h2>
          <hr class="botm-line">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
          <div class="thumbnail">
            <img src="img/standard.jpg" alt="..." class="team-img">
            <div class="caption">
              <h3>STANDARDS</h3>
              <p>Entdecken Sie die gesamte Palette der Standard-Container.<br> 
              Finden Sie den Beh�lter, der Ihren Bed�rfnissen entspricht.<br>SS Sie z�gern zwischen mehreren Containern, </p>
              <ul class="list-inline">
                <li><a href="#">�berpr�fen Sie die Abmessungen unserer Container</i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
          <div class="thumbnail">
            <img src="img/FRIGORIFIQUES.jpg" alt="..." class="team-img">
            <div class="caption">
              <h3>FRIGORIFIQUES</h3>
              <p>D�couvrez la gamme compl�te de containers standards.<br> 
              Trouvez le container qui correspond � votre besoin.<br>SS Vous h�sitez entre plusieurs containers, </p>
              <ul class="list-inline">
                <li><a href="#">consultez les dimensions de nos containers</i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
          <div class="thumbnail">
            <img src="img/stockage.jpg" alt="..." class="team-img">
            <div class="caption">
              <h3>Conteneur de stockage</h3>
              <p>D�couvrez la gamme compl�te de containers standards.<br> 
              Trouvez le container qui correspond � votre besoin.<br>SS Vous h�sitez entre plusieurs containers, </p>
              <ul class="list-inline">
                <li><a href="#">consultez les dimensions de nos containers</i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
          <div class="thumbnail">
            <img src="img/porte.jpg" alt="..." class="team-img">
            <div class="caption">
              <h3>Conteneur � double porte</h3>
              <p>D�couvrez la gamme compl�te de containers standards.<br> 
              Trouvez le container qui correspond � votre besoin.<br>SS Vous h�sitez entre plusieurs containers, </p>
              <ul class="list-inline">
                <li><a href="#">consultez les dimensions de nos containers</i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ doctor team-->
  <!--testimonial-->
  <section id="testimonial" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="ser-title">see what clients are saying?</h2>
          <hr class="botm-line">
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="testi-details">
            <!-- Paragraph -->
            <p></p>
          </div>
          <div class="testi-info">
            <!-- User Image -->
            <a href="#"><img src="img/thumb.png" alt="" class="img-responsive"></a>
            <!-- User Name -->
            <h3>Nouamane<span>EL gueddari</span></h3>
          </div>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="testi-details">
            <!-- Paragraph -->
            <p></p>
          </div>
          <div class="testi-info">
            <!-- User Image -->
            <a href="#"><img src="img/thumb.png" alt="" class="img-responsive"></a>
            <!-- User Name -->
            <h3>Nouamane<span>El gueddari</span></h3>
          </div>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="testi-details">
            <!-- Paragraph -->
            <p></p>
          </div>
          <div class="testi-info">
            <!-- User Image -->
            <a href="#"><img src="img/thumb.png" alt="" class="img-responsive"></a>
            <!-- User Name -->
            <h3>Nouamane<span>El gueddari</span></h3>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ testimonial-->
  <!--cta 2-->
  <section id="cta-2" class="section-padding">
    <div class="container">
      <div class=" row">
        <div class="col-md-2"></div>
        <div class="text-right-md col-md-4 col-sm-4">
          <h2 class="section-title white lg-line">Haben Sie noch Fragen?<br>
		     Unsere Container-Experten stehen Ihnen zur Verf�gung!</h2>
        </div>
        <div class="col-md-4 col-sm-5">
         	 Wir beraten Sie gerne pers�nlich vor Ort, telefonisch oder per E-Mail.
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </section>
  <!--cta-->
  <!--contact-->
  <section id="contact" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="ser-title">Kontaktiere uns</h2>
          <hr class="botm-line">
        </div>
        <div class="col-md-4 col-sm-4">
          <h3>Kontaktinformation</h3>
          <div class="space"></div>
          <p><i class="fa fa-map-marker fa-fw pull-left fa-2x"></i><br> Meknes, majane 50050</p>
          <div class="space"></div>
          <p><i class="fa fa-envelope-o fa-fw pull-left fa-2x"></i>Company.Bachlor@gmail.com</p>
          <div class="space"></div>
          <p><i class="fa fa-phone fa-fw pull-left fa-2x"></i>+212 61 511 1234</p>
        </div>
        <div class="col-md-8 col-sm-8 marb20">
          <div class="contact-info">
            <div class="space"></div>
            <div id="sendmessage">Ihre Nachricht wurde gesendet. Vielen Dank!</div>
            <div id="errormessage"></div>
            <form action="" method="post" role="form" class="contactForm">
              <div class="form-group">
                <input type="text" name="name" class="form-control br-radius-zero" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="email" class="form-control br-radius-zero" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control br-radius-zero" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control br-radius-zero" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validation"></div>
              </div>

              <div class="form-action">
                <button type="submit" class="btn btn-form">Nachricht senden</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ contact-->
  <!--footer-->
  <footer id="footer">
    <div class="top-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4 marb20">
            <div class="ftr-tle">
              <h4 class="white no-padding">About Us</h4>
            </div>
            <div class="info-sec">
              <p>Praesent convallis tortor et enim laoreet, vel consectetur purus latoque penatibus et dis parturient.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 marb20">
            <div class="ftr-tle">
              <h4 class="white no-padding">Quick Links</h4>
            </div>
            <div class="info-sec">
              <ul class="quick-info">
                <li><a href="index.html"><i class="fa fa-circle"></i>Home</a></li>
                <li><a href="#service"><i class="fa fa-circle"></i>Service</a></li>
                <li><a href="#contact"><i class="fa fa-circle"></i>Appointment</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 marb20">
            <div class="ftr-tle">
              <h4 class="white no-padding">Follow us</h4>
            </div>
            <div class="info-sec">
              <ul class="social-icon">
                <li class="bglight-blue"><i class="fa fa-facebook"></i></li>
                <li class="bgred"><i class="fa fa-google-plus"></i></li>
                <li class="bgdark-blue"><i class="fa fa-linkedin"></i></li>
                <li class="bglight-blue"><i class="fa fa-twitter"></i></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-line">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            Â© Copyright Medilab Theme. All Rights Reserved
            <div class="credits">
              <!--
                All the links in the footer should remain intact.
                You can delete the links only if you purchased the pro version.
                Licensing information: https://bootstrapmade.com/license/
                Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Medilab
              -->
              Designed by <a href="https://bootstrapmade.com/">BootstrapMade.com</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--/ footer-->

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>

</body>

</html>
