<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
	<title>Location de conteneurs- Bachelor in Computer Engineering</title>

	<link rel="shortcut icon" href="assets/images/gt_favicon.png">
	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"> 
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700"> 
	<link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>

<!-- Header -->
<header class="header">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-push-2 text-center">
				<h1>Containervermietung</h1>
				<h2>Die Anmietung eines Containers bei uns ist einfach und unkompliziert.</h2>
				<p class="lead">
					Buchen und bezahlen Sie online Ihren Baucontainer (tempor�rer Mietcontainer).
				</p>Rufen Sie uns an, um Ihren M�llcontainer und das Recycling (Dauermietcontainer) zu reservieren.</p>
					<br>
				
				<form class="form-inline"  action="Login.php" method="post">
				    <div>
					<div class="form-group"><input type="text" class="form-control input-lg" name="email" placeholder="Email"></div>
					<div class="form-group"><input type="password" class="form-control input-lg" name="password" placeholder="Password"></div>
					<button type="submit" class="btn btn-lg btn-default">login</button>
					</div>
				</form>
				<p class="small text-muted"> F�r Lieferungen au�erhalb unseres Territoriums fallen zus�tzliche Geb�hren an. Ein Kundendienstmitarbeiter wird Kunden kontaktieren, die Projekte au�erhalb unseres bedienten Gebiets haben</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div id="illustration">
					<img src="assets/images/ap-screenshot.png" alt="" >
				</div>
			</div>
		</div>
	</div>
</header>
<!-- /Header -->
<!-- Content -->
<div class="container">
        <div class="row centered-form">
        <div class="col-xs-6 col-sm-12 col-md-8 col-sm-offset-2 col-md-offset-2">
        	<div class="panel panel-default">
        		<div class="panel-heading">
			    		<h3 class="panel-title">Please sign up </h3>
			 			</div>
			 			<div class="panel-body">
			    		<form action="subscribe.php" method="post">
			    			<div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			                <input type="text" name="first_name"  class="form-control input-sm" placeholder="First Name">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="text" name="last_name" class="form-control input-sm" placeholder="Last Name">
			    					</div>
			    				</div>
			    			</div>
                            <div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="email" name="email"  class="form-control input-sm" placeholder="Email Address">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="text" name="adresse" class="form-control input-sm" placeholder="Adresse">
			    					</div>
			    				</div>
			    			</div>

                            <div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="text" name="fax" class="form-control input-sm" placeholder="Fax">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="text" name="tel"  class="form-control input-sm" placeholder="Tel">
			    					</div>
			    				</div>
			    			</div>
			    			<div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="password" name="password"  class="form-control input-sm" placeholder="Password">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="password" name="password_confirmation"  class="form-control input-sm" placeholder="Confirm Password">
			    					</div>
			    				</div>
			    			</div>
			    			
			    			<input type="submit" name="register" value="register" class="btn btn-info btn-block">
			    		
			    		</form>
			    	</div>
	    		</div>
    		</div>
    	</div>
    </div>

<p class="small text-muted text-center">Copyright &copy; 2018, nouamane el gueddari. </p>
<br>



<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="assets/js/template.js"></script>

</body>
</html>