<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<title>Les Fournisseurs</title>
</head>
<body>
<p></p>
<div class="container col-md-6 col-md-offset-3 ">
		<div class="well well-sm">
   				 <div class="panel-heading"> Best�tigung der �nderung</div>
  		</div>
			<form action="update_dem_location.php" method="get">
				<div class="panel-primary">
				<table class="table table-striped">	
						<tr> 
							<th><label>ID:</label> </th> <th><input type="text" class="form-control" name="Id" value= ${model.demande.id_demande} readonly="readonly"> </th>
						</tr>		
					 	<tr> 
							<th><label>ID_Kund:</label> </th> <th><input type="text" class="form-control" name="ID_Kund" value=${model.demande.id_client}> </th>
						</tr>	
						<tr> 
							<th><label> ID_Container: </label></th> <th> <input type="text" class="form-control"  name="Id_conteneur" value=${model.demande.id_conteneur}> </th>
						</tr>
						<tr> 
							<th><label>Mietantragsdatum:</label> </th> <th> <input type="text" class="form-control"  name="date_demande" value=${model.demande.date_demande_loc}> </th>
						</tr>
						<tr> 
							<th><label>Mietbeginndatum:</label> </th> <th> <input type="text" class="form-control"  name="date_debut" value=${model.demande.date_debut_loc}> </th>
						</tr>
						<tr> 
							<th> <label>Ende des Mietdatums:</label> </th> <th> <input type="text" class="form-control"  name="date_fin" value=${model.demande.date_fin_loc}> </th>
						</tr>
						<tr> 
							<th> <label>Menge:</label> </th> <th> <input type="text" class="form-control"  name="menge" value=${model.demande.quantite}> </th>
						</tr>		
						<tr> 
							<th> <label>Lieferadresse:</label> </th> <th> <input type="text" class="form-control"  name="adresse" value=${model.demande.adresse_livraison}> </th>
						</tr>	
						<tr> 
							<th> <label>Postleitzahl:</label> </th> <th> <input type="text" class="form-control"  name="Postleitzahl" value=${model.demande.code_postal}> </th>
						</tr>	
						<tr> 
							<th> <label>Stadt:</label> </th> <th> <input type="text" class="form-control"  name="ville" value=${model.demande.ville}> </th>
						</tr>	
				</table>		
					
				</div>				
				
				<div class="container col-md-3  ">
    				<input type="submit" onclick="return confirm('�tes-vous s�r de bien vouloir modifier cet �l�ment?');" name ="modifier"  value="modifier" class="btn btn-primary" />
    				
  				</div>
  					<div class="container col-md-3  ">
  						<input type="submit" class="btn btn-primary" name="annuler" value="Annuler"/>
  					</div>
  					
       </form>

       </div>
  
</body>
</html>