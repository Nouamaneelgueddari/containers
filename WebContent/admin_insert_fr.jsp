<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<title>Les Fournisseurs</title>
</head>
<body>
<div class="container col-md-6 col-md-offset-3 ">
		<div class="well well-sm">
   				 <div class="panel-body">Ajout Fournisseur</div>
  		</div>
			<form action="insert_fr.php" method="get">		
					  <table class="table ">						
						<tr> 
							<th> <label >Nom:</label> </th> <th><input type="text" class="form-control" name="nom" value=""> </th>
						</tr>	
						<tr> 
							<th> <label  >Pr�nom:</label> </th> <th> <input type="text" class="form-control"  name="prenom" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >Email:</label> </th> <th> <input type="email" class="form-control"  name="email" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >Adresse:</label> </th> <th> <input type="text" class="form-control"  name="adresse" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >Tel:</label> </th> <th> <input type="text" class="form-control"  name="tel" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >Fax:</label> </th> <th> <input type="text" class="form-control"  name="fax" value=""> </th>
						</tr>
						
					 </table>
					
					<div class="container col-md-3  ">
	       				<input type="submit" name="save" value="save"  class="btn btn-primary">
	      		    </div>			
      		 </form>
		       <div class="container col-md-1 col-md-offset-6 ">
			       <form action="consulter_fr.php"  method="post">
			       		<input type="submit" name="consulter" value="consulter"  class="btn btn-primary">
			       </form>
		       </div>
       </div>

  
</body>
</html>