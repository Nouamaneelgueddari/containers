<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<title>Containers</title>
</head>
<body>
<div class="container col-md-6 col-md-offset-3 ">
		<div class="well well-sm">
   				 <div class="panel-body">F�gen Sie Container hinzu</div>
  		</div>
			<form action="insert.php" method="get">
				<div class="panel-primary">
					  <table class="table ">					
						<tr> 
							<th> <label >type:</label> </th> <th><input type="text" class="form-control" name="type" value=""> </th>
						</tr>	
						<tr> 
							<th> <label  >Breite::</label> </th> <th> <input type="text" class="form-control"  name="largeur" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >H�he:</label> </th> <th> <input type="text" class="form-control"  name="hauteur" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >L�nge:</label> </th> <th> <input type="text" class="form-control"  name="longueur" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >Breite der T�r:</label> </th> <th> <input type="text" class="form-control"  name="largeur_porte" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >H�he der T�r:</label> </th> <th> <input type="text" class="form-control"  name="longueur_porte" value=""> </th>
						</tr>
						
						<tr> 
							<th> <label  >L�nge der T�r:</label> </th> <th> <input type="text" class="form-control"  name="hauteur_porte" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >Gewicht:</label> </th> <th> <input type="text" class="form-control"  name="poids" value=""> </th>
						</tr>
						
						<tr> 
							<th> <label  >Kapazit�t :</label> </th> <th> <input type="text" class="form-control"  name="capacite" value=""> </th>
						</tr>
						<tr> 
							<th> <label  >Preis:</label> </th> <th> <input type="text" class="form-control"  name="prix" value=""> </th>
						</tr>
						<!-- 
						<tr> 
							<th> <label  >Image:</label> </th> <th> <input class="form-control"   type="file" name="pic" accept="image/*"></th>
						</tr>- -->
					
					 </table>
				</div>
				<div class="container col-md-3  ">
       				<input type="submit" name="save" value="Save"  class="btn btn-primary">
      		    </div>
					
					
       </form>
       <div class="container col-md-1 col-md-offset-6 ">
       <form action="consulter.php"  method="get">
       		<input type="submit" name="consulter" value="consulter"  class="btn btn-primary">
       </form>
       </div>
       </div>
  
</body>
</html>