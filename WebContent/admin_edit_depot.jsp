<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<title>Les Conteneurs</title>
</head>
<body>
<p></p>
<div class="container col-md-6 col-md-offset-3 ">
		<div class="well well-sm">
   				 <div class="panel-heading"> Confirmation de modification</div>
  		</div>
			<form action="update_depot.php" method="get">
				<div class="panel-primary">
				<table class="table table-striped">			
					 	<tr> 
							<th> <label >ID der lager:</label> </th> <th><input type="text" class="form-control" name="id" value=${model.dep.id_depot} readonly="readonly"> </th>
						</tr>	
						<tr> 
							<th> <label  >Anzahl der Pl�tze :</label> </th> <th> <input type="text" class="form-control"  name="nbrPlace" value=${model.dep.nb_place}> </th>
						</tr>
						<tr> 
							<th> <label  >Adresse:</label> </th> <th> <input type="text" class="form-control"  name="adresse" value=${model.dep.adresse}> </th>
						</tr>
						<tr> 
							<th> <label  >Stadt:</label> </th> <th> <input type="text" class="form-control"  name="ville" value=${model.dep.ville}> </th>
						</tr>
						<tr> 
							<th> <label  >Postleitzahl:</label> </th> <th> <input type="text" class="form-control"  name="codePostal" value=${model.dep.code_postal}> </th>
						</tr>		
				<tr> 			
				</table>		
					
				</div>				
				
				<div class="container col-md-3  ">
    				<input type="submit" onclick="return confirm('�tes-vous s�r de bien vouloir modifier cet �l�ment?');" name ="modifier"  value="Ver�nderung" class="btn btn-primary" />
  				</div>
  					<div class="container col-md-3  ">
  						<input type="submit" name="annuler"  value="Stornieren"  class="btn btn-primary" />
  					</div>	
       </form>
       </div>
</body>
</html>