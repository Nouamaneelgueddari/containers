<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<title>Containers</title>
</head>
<body>
<p></p>
<div class="container col-md-6 col-md-offset-3 ">
		<div class="well well-sm">
   				 <div class="panel-heading">Best�tigung der �nderung</div>
  		</div>
			<form action="update_conteneur.php" method="get">
				<div class="panel-primary">
				<table class="table table-striped">			
					 	<tr> 
							<th><label>ID:</label> </th> <th><input type="text" class="form-control" name="id" value=${model.c.id} readonly="readonly"> </th>
						</tr>	
						<tr> 
							<th> <label>id_lager:</label> </th> <th> <input type="text" class="form-control"  name="id_depot" value=${model.c.id_depot}> </th>
						</tr>
						<tr> 
							<th> <label>type:</label> </th> <th> <input type="text" class="form-control"  name="type" value=${model.c.type}> </th>
						</tr>
						<tr> 
							<th> <label>Breite:</label> </th> <th> <input type="text" class="form-control"  name="largeur" value=${model.c.largeur}> </th>
						</tr>
						<tr> 
							<th> <label>H�he:</label> </th> <th> <input type="text" class="form-control"  name="hauteur" value=${model.c.hauteur}> </th>
						</tr>
						<tr> 
							<th> <label>L�nge:</label> </th> <th> <input type="text" class="form-control"  name="longueur" value=${model.c.longueur}> </th>
						</tr>
						<tr> 
							<th> <label>Breite der T�r:</label> </th> <th> <input type="text" class="form-control"  name="largeur_porte" value=${model.c.largeur_porte}> </th>
						</tr>
						<tr> 
							<th> <label>H�he der T�r:</label> </th> <th> <input type="text" class="form-control"  name="hauteur_porte" value=${model.c.hauteur_porte}> </th>
						</tr>
						<tr> 
							<th> <label>L�nge der T�r:</label> </th> <th> <input type="text" class="form-control"  name="longueur_porte" value=${model.c.longueur_porte}> </th>
						</tr>
						<tr> 
							<th> <label>Gewicht:</label> </th> <th> <input type="text" class="form-control"  name="Poids" value=${model.c.poids}> </th>
						</tr>	
						<tr> 
							<th> <label>Kapazit�t:</label> </th> <th> <input type="text" class="form-control"  name="Capacite" value=${model.c.capacite}> </th>
						</tr>	
						<tr> 
							<th> <label>Preis:</label> </th> <th> <input type="text" class="form-control"  name="Prix" value=${model.c.prix}> </th>
						</tr>				
				<tr> 			
				</table>		
					
				</div>				
				
				<div class="container col-md-3  ">
    				<input type="submit" onclick="return confirm('�tes-vous s�r de bien vouloir modifier cet �l�ment?');" name ="modifier"  value="Ver�nderung" class="btn btn-primary" />
  				</div>
  				<div class="container col-md-3  ">
  						<input type="submit" class="btn btn-primary" name="Annuler_conteneur " value="stornieren"/>
  				</div>	
       </form>
       </div>
</body>
</html>