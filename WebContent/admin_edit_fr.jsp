<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<title>Les Fournisseurs</title>
</head>
<body>
<p></p>
<div class="container col-md-6 col-md-offset-3 ">
		<div class="well well-sm">
   				 <div class="panel-heading"> Confirmation de modification</div>
  		</div>
			<form action="update_fr.php" method="get">
				<div class="panel-primary">
				<table class="table table-striped">	
						<tr> 
							<th> <label >ID:</label> </th> <th><input type="text" class="form-control" name="id" value=${model.fr.id_fournisseur} readonly="readonly"> </th>
						</tr>		
					 	<tr> 
							<th> <label >Name:</label> </th> <th><input type="text" class="form-control" name="nom" value=${model.fr.nom}> </th>
						</tr>	
						<tr> 
							<th> <label  >Vorname:</label> </th> <th> <input type="text" class="form-control"  name="prenom" value=${model.fr.prenom}> </th>
						</tr>
						<tr> 
							<th> <label  >Email:</label> </th> <th> <input type="email" class="form-control"  name="email" value=${model.fr.email}> </th>
						</tr>
						<tr> 
							<th> <label  >Adresse:</label> </th> <th> <input type="text" class="form-control"  name="adresse" value=${model.fr.adresse}> </th>
						</tr>
						<tr> 
							<th> <label  >Tel:</label> </th> <th> <input type="text" class="form-control"  name="tel" value=${model.fr.tel}> </th>
						</tr>
						<tr> 
							<th> <label  >Fax:</label> </th> <th> <input type="text" class="form-control"  name="fax" value=${model.fr.fax}> </th>
						</tr>		
				</table>		
					
				</div>				
				
				<div class="container col-md-3  ">
    				<input type="submit" onclick="return confirm('�tes-vous s�r de bien vouloir modifier cet �l�ment?');" name ="modifier"  value="modifier" class="btn btn-primary" />
    				
  				</div>
  					<div class="container col-md-3  ">
  						<input type="submit" class="btn btn-primary" name="annuler" value="Annuler"/>
  					</div>
  					
       </form>

       </div>
  
</body>
</html>